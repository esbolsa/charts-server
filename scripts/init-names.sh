release="2.5.1"

sudo rm -rf /opt/esbolsa-charts/logs/esBolsaNames*

sudo java8 -Xms3G -Xmx4G -Dweblogic.security.SSL.minimumProtocolVersion=TLSv1.1 -Dhttps.protocols=TLSv1.2 -Ddeployment.security.TLSv1.2=true -Dprop=/opt/esbolsa-charts/conf/esbolsa-names.properties -Dfile.encoding=UTF-8 -jar /opt/esbolsa-charts/jars/esbolsa-names-${release}-jar-with-dependencies.jar &
