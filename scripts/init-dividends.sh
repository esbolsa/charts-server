release="2.2.3"

sudo rm -rf /opt/esbolsa-charts/logs/esBolsaDividends*

sudo /usr/lib/jvm/jre-1.8.0-openjdk.x86_64/bin/java -Xms3G -Xmx4G -Dprop=/opt/esbolsa-charts/conf/esbolsa-dividends.properties -Dfile.encoding=UTF-8 -jar /opt/esbolsa-charts/jars/esbolsa-dividends-${release}-jar-with-dependencies.jar &
