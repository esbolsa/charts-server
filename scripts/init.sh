
release="5.8.7"

sudo rm -rf /opt/esbolsa-charts/logs/esBolsaCharts*

# remove api data
redis-cli KEYS "/api*" | xargs redis-cli DEL

if [[ "$2" != "" ]]; then
    WORKER="$2"
else
    WORKER="all"
fi

if [ "$1" == "ranking" ]; then
	sudo java -Xms3G -Xmx4G -Dprop=/opt/esbolsa-charts/conf/esbolsa-charts.properties -Dranking=true -Dworker=${WORKER} -DindividualTicker="$3" -DupdateVolume="$4" -Dfile.encoding=UTF-8 -jar /opt/esbolsa-charts/jars/charts-${release}-jar-with-dependencies.jar &
else
	sudo java -Xms3G -Xmx4G -Dprop=/opt/esbolsa-charts/conf/esbolsa-charts.properties -Dworker=${WORKER} -DindividualTicker="$3" -DupdateVolume="$4" -Dfile.encoding=UTF-8 -jar /opt/esbolsa-charts/jars/charts-${release}-jar-with-dependencies.jar &
fi



# remove api data
redis-cli KEYS "/api*" | xargs redis-cli DEL
