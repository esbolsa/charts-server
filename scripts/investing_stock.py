import investpy
import sys

ticker = sys.argv[1]

df = investpy.get_stock_historical_data(stock=ticker,
                                        country='United States',
                                        from_date='01/01/2010',
                                        to_date='01/01/2025',
                                        as_json=True)

