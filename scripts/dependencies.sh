release="3.1.3"

sudo yum update --assumeyes

sudo yum upgrade --assumeyes

sudo yum groupinstall "Development tools" --assumeyes

sudo yum remove java-1.7.0-openjdk --assumeyes

sudo yum install java-1.8.0 --assumeyes

sudo yum install nano --assumeyes


echo "*****************************************"
echo " 1. Prerequisites: Install updates, set time zones, install GCC and make"
echo "*****************************************"
#sudo yum -y update
#sudo ln -sf /usr/share/zoneinfo/America/Los_Angeles \/etc/localtime
#sudo yum -y install gcc gcc-c++ make 
echo "*****************************************"
echo " 2. Download, Untar and Make Redis 2.6"
echo "*****************************************"
cd /usr/local/src
sudo wget http://download.redis.io/releases/redis-3.2.0.tar.gz
sudo tar xzf redis-3.2.0.tar.gz
sudo rm redis-3.2.0.tar.gz -f
cd redis-3.2.0
sudo make distclean
sudo make
echo "*****************************************"
echo " 3. Create Directories and Copy Redis Files"
echo "*****************************************"
sudo mkdir /etc/redis /var/lib/redis
sudo cp src/redis-server src/redis-cli /usr/local/bin
echo "*****************************************"
echo " 4. Configure Redis.Conf"
echo "*****************************************"
echo " Edit redis.conf as follows:"
echo " 1: ... daemonize yes"
echo " 2: ... bind 127.0.0.1"
echo " 3: ... dir /var/lib/redis"
echo " 4: ... loglevel notice"
echo " 5: ... logfile /var/log/redis.log"
echo "*****************************************"
sudo sed -e "s/^daemonize no$/daemonize yes/" -e "s/bind 127.0.0.1$/bind 0.0.0.0/" -e "s/^dir \.\//dir \/var\/lib\/redis\//" -e "s/^loglevel verbose$/loglevel notice/" -e "s/^logfile stdout$/logfile \/var\/log\/redis.log/" redis.conf | sudo tee /etc/redis/redis.conf
echo "*****************************************"
echo " 5. Download init Script"
echo "*****************************************"
sudo wget https://raw.github.com/saxenap/install-redis-amazon-linux-centos/master/redis-server
echo "*****************************************"
echo " 6. Move and Configure Redis-Server"
echo "*****************************************"
sudo mv redis-server /etc/init.d
sudo chmod 755 /etc/init.d/redis-server
echo "*****************************************"
echo " 7. Auto-Enable Redis-Server"
echo "*****************************************"
sudo chkconfig --add redis-server
sudo chkconfig --level 345 redis-server on
echo "*****************************************"
echo " 8. Start Redis Server"
echo "*****************************************"
sudo service redis-server start






#sudo wget -r --no-parent -A 'epel-release-*.rpm' http://dl.fedoraproject.org/pub/epel/7/x86_64/e/

#sudo rpm -Uvh dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-*.rpm

#sudo yum install redis --assumeyes

#sudo sed -i -e 's/bind 127.0.0.1/bind 0.0.0.0/g' -- /etc/redis/redis.conf

#sudo systemctl start redis.service

sudo git clone https://esbolsa:3sb0ls42011#bb@bitbucket.org/esbolsa/charts-server.git /opt/esbolsa-charts

sudo chmod 755 /opt/esbolsa-charts/scripts/init.sh

sudo chmod 755 /opt/esbolsa-charts/scripts/init-dividends.sh



sudo yum install nodejs npm --enablerepo=epel --assumeyes
cd /opt/esbolsa-charts/server
sudo npm install -g
sudo npm install
sudo npm install forever -g
NODE_ENV=production forever start app.js
cd
