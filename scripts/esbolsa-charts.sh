#!/bin/bash

java_path="java"

# Minimum memory allocated to JVM
XMS="256M"

# Maximum memory allocated to JVM
XMX="400M"

# installation root
root_dir="/opt/esbolsa-charts/"

# jar file name
jar_file="${root_dir}jars/ChartsWithDependencies.jar"

# conf file
conf_file="${root_dir}conf/esbolsa-charts.properties"

# remove logs
rm -rf /opt/esbolsa-charts/logs/esBolsa*

echo "$0: Starting esBolsa Charts Server "

java_opts="-Xms${XMS} -Xmx${XMX} -Dprop=${conf_file} -Dfile.encoding=UTF-8"

$java_path $java_opts -jar $jar_file &

# remove api data
redis-cli KEYS "/api*" | xargs redis-cli DEL

# End of script
