var express = require('express'),
    router = express.Router(),
    exec = require('child_process').exec,
    spawnSync = require("child_process").spawnSync,
    request = require('request'),
    cheerio = require("cheerio"),
    _ = require('lodash');
    const {Builder, Browser, By, Key, until} = require('selenium-webdriver')
    const chrome = require('selenium-webdriver/chrome');

module.exports = function (app) {
    app.use('/', router);
    app.use('/log/tail/:rows', router);
    app.use('/api/yahoo/:ticker', router);
    app.use('/log/dividends/tail/:rows', router);
    app.use('/exec/isFinished', router);
    app.use('/exec/isFinishedDividends', router);
    app.use('/exec/start', router);
    app.use('/exec/startDividends', router);
    app.use('/exec/clearCache', router);
    app.use('/api/wsj/:ticker', router);
};


router.get('/api/wsj/:ticker', function (req, res, next) {
    var url = "http://quotes.wsj.com/COUNTRY/TICKER";
    var ticker_bloomberg = req.params.ticker;
    console.log('ticker_bloomberg', ticker_bloomberg)
    var _ticker = translateBloombergToWSJ(ticker_bloomberg);
    console.log('_ticker',_ticker)
    var ticker = _ticker.substring(0, _ticker.length - 3);
    var country = "/" + _ticker.substring(_ticker.length - 2, _ticker.length);
    if (country == "/US") country = "";
    console.log('URL', getUrl(ticker, country))

    // let options = new chrome.Options();
    // options.addArguments("--headless"); // Configura Chrome en modo headless
    // options.addArguments("--disable-gpu"); 
    // ;(async function example() {
    //     let driver = await new Builder().forBrowser(Browser.CHROME).setChromeOptions(options).build();
    //     try {
    //         const url = getUrl(ticker, country)
    //         console.log('url', url)
    //       await driver.get(url);
    //       const body = await driver.findElement(By.tagName('body'));
    //       const html = await body.getAttribute('innerHTML');
    //     console.log('HTML del body:', html);
    //     } finally {
    //       await driver.quit();
    //     }
    //   })();


    request({ url: getUrl(ticker, country),headers: {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/64.0.3282.167 Chrome/64.0.3282.167 Safari/537.36", },
        "method": "GET",
        "mode": "cors"
        } , function (err, resp, body) {
        console.log('resp', body)
        if (err || resp.statusCode != 200) {
            console.log(err);
        }

        if (!body) return next()

        var json = body.substring(body.indexOf("window.__STATE__") + 18, body.indexOf("[\"WSJTheme\"]}}}};") + 16);
        try {
            var obj = JSON.parse(json)
        } catch (error) {
            return null
        }
        var sector = null
        var name = null
        var dividend = null
        var exDividend = null
        if (obj["context"]
            && obj["context"]["tickerData"]
            && obj["context"]["tickerData"]["IndustryClassification"]
            && obj["context"]["tickerData"]["IndustryClassification"]["Industry"]
            && obj["context"]["tickerData"]["IndustryClassification"]["Industry"]["DisplayName"])
            sector = obj["context"]["tickerData"]["IndustryClassification"]["Industry"]["DisplayName"].trim()
        if (obj["context"]
            && obj["context"]["tickerData"]
            && obj["context"]["tickerData"]["Instrument"]
            && obj["context"]["tickerData"]["Instrument"]["CommonName"]
            && obj["context"]["tickerData"]["Instrument"]["CommonName"])
            name = obj["context"]["tickerData"]["Instrument"]["CommonName"].trim()
        if (obj["context"]
            && obj["context"]["tickerData"]
            && obj["context"]["tickerData"]["Financials"]
            && obj["context"]["tickerData"]["Financials"]["LastDividendPerShare"]
            && obj["context"]["tickerData"]["Financials"]["LastDividendPerShare"]["Value"])
            dividend = obj["context"]["tickerData"]["Financials"]["LastDividendPerShare"]["Value"]
        if (obj["context"]
            && obj["context"]["tickerData"]
            && obj["context"]["tickerData"]["Financials"]
            && obj["context"]["tickerData"]["Financials"]["LastDividendExDateTime"])
            exDividend = obj["context"]["tickerData"]["Financials"]["LastDividendExDateTime"]
        if (exDividend) exDividend = Number(exDividend.substring(6, exDividend.indexOf("+")))




        var $ = cheerio.load(body);
        var name = $('.WSJTheme--title--8uZ7oFS7 h1').text();
        console.log('-', name, sector, dividend, exDividend)
        //console.log($(".WSJTheme--data--2GDojo-q"))
        //var sector = $(".WSJTheme--data--2GDojo-q").get(1).children[0].data.trim();

        var redis = req.app.get('redis');
        var c = req.params.ticker.substring(req.params.ticker.length - 2, req.params.ticker.length);
        console.log('sector', req.params.ticker, `sector_${getZone(c.toUpperCase())}_${sector}`.split('/').join('-'))
        //console.log(`sector_${getZone(c.toUpperCase())}_${sector}`.split('/').join('-'))
        /*res.send({
            name: name,
            sectorId: null 
        });  */
        redis.get(`sector_${getZone(c.toUpperCase())}_${sector}`.split('/').join('-'), function (err, result) {
            if (err) {
                console.log(err)
                return next(err);
            }
            res.send({
                name: name,
                sectorId: result,
                sectorName: sector,
                dividend: dividend,
                exDividend: exDividend
            });
        });
    });
});


router.get('/api/yahoo/:ticker', function (req, res, next) {
    console.log('entra', req.params.ticker)
    var ticker = req.params.ticker
    

    var pythonProcess = spawnSync('pwd', { encoding: 'utf8' })
    console.log(pythonProcess.stdout)
    var pythonProcess = spawnSync('conda activate py39', { encoding: 'utf8' })
    var pythonProcess = spawnSync('python3', ["app/api/yahoo.py", ticker], { encoding: 'utf8' })
    return res.send(pythonProcess.stdout)
});

router.get('/log/tail/:rows', function (req, res, next) {
    var rows = req.params && req.params.rows ? req.params.rows : 10;
    exec('tail ../logs/esBolsaCharts.log -n ' + rows, function (err, stdout, stderr) {
        if (err) return console.error('exec error', err);
        res.send(stdout.replaceAll('\n', '<br/>'));
    });
});


router.get('/log/dividends/tail/:rows', function (req, res, next) {
    var rows = req.params && req.params.rows ? req.params.rows : 10;
    exec('tail ../logs/esBolsaDividends.log -n ' + rows, function (err, stdout, stderr) {
        if (err) return console.error('exec error', err);
        res.send(stdout.replaceAll('\n', '<br/>'));
    });
});

router.get('/exec/isFinished', function (req, res, next) {
    exec('(ls ../logs/esBolsaCharts.log >> /dev/null 2>&1 && echo yes) || echo no', function (err, stdout, stderr) {
        if (err) {
            console.error('exec error', err);
        }
        if (_.includes(stdout, 'no')) return res.send(true)
        exec('tail ../logs/esBolsaCharts.log -n 10', function (err, stdout, stderr) {
            if (err) {
                console.error('exec error', err);
            }
            res.send(err || _.includes(stdout, 'The download is finished'));
        });
    });
});

router.get('/exec/isFinishedDividends', function (req, res, next) {
    exec('(ls ../logs/esBolsaDividends.log >> /dev/null 2>&1 && echo yes) || echo no', function (err, stdout, stderr) {
        if (err) {
            console.error('exec error', err);
        }
        if (_.includes(stdout, 'no')) return res.send(true)
        exec('tail ../logs/esBolsaDividends.log -n 10', function (err, stdout, stderr) {
            if (err) {
                console.error('exec error', err);
                return next(err);
            }
            res.send(_.includes(stdout, 'Download successfull'));
        });
    });
});

router.post('/exec/start', function (req, res, next) {
    exec('tail ../logs/esBolsaCharts.log -n 10', function (err, stdout, stderr) {
        if (err || _.includes(stdout, 'The download is finished')) {
            var script = (req.query.ranking == 'true'
            ? './../scripts/init.sh ' + 'ranking ' + (req.query.worker || 'all') + ' ' + (req.query.individualTicker || '') + ' ' + (req.query.updateVolume)
            : './../scripts/init.sh false ' + (req.query.worker || 'all') + ' ' + (req.query.individualTicker || '')) + ' ' + (req.query.updateVolume)
            + (req.query.individualTicker ? "; ./../scripts/yahoo-dividends.sh " + (req.query.individualTicker || '') : "") + ' ' + (req.query.updateVolume)

            console.log('exec script ->', req.query.ranking == 'true' ? './../scripts/init.sh ' + 'ranking ' + (req.query.worker || 'all') : './../scripts/init.sh false ' + (req.query.worker || 'all'), 'individualticker->', req.query.individualTicker);
            console.log('SCRIPT ->', script)
            exec(script,
                function (error, stdout, stderr) {
                    if (err) {
                        console.error('exec error', err);
                        return next(err);
                    }
                    res.send(200);
                });
        }
    });
});

router.post('/exec/startDividends', function (req, res, next) {
    exec('(ls ../logs/esBolsaDividends.log >> /dev/null 2>&1 && echo yes) || echo no', function (err, stdout, stderr) {
        if (err) {
            console.error('exec error', err);
        }
        if (_.includes(stdout, 'no')) return res.send(true)
        exec('tail ../logs/esBolsaDividends.log -n 10', function (err, stdout, stderr) {
            if (err || _.includes(stdout, 'Download successfull')) {
                exec('./../scripts/init-dividends.sh ', function (error, stdout, stderr) {
                    if (err) {
                        console.error('exec error', err);
                        return next(err);
                    }
                    res.send(200);
                });
            }
        });
    });
});

router.post('/exec/clearCache', function (req, res, next) {
    exec('redis-cli KEYS "/api*" | xargs redis-cli DEL', function (err, stdout, stderr) {
        if (err) {
            console.error('exec error', err);
            return next(err);
        }
        res.send(200);
    });
});

String.prototype.replaceAll = function (s, r) { return this.split(s).join(r) }


function translateBloombergToWSJ (ticker) {
    if ((function (str, searchString) { var pos = str.length - searchString.length; var lastIndex = str.indexOf(searchString, pos); return lastIndex !== -1 && lastIndex === pos; })(ticker, ":SM"))
        return ticker.split(":SM").join(":ES");
    else if ((function (str, searchString) { var pos = str.length - searchString.length; var lastIndex = str.indexOf(searchString, pos); return lastIndex !== -1 && lastIndex === pos; })(ticker, ":FP"))
        return ticker.split(":FP").join(":FR");
    else if ((function (str, searchString) { var pos = str.length - searchString.length; var lastIndex = str.indexOf(searchString, pos); return lastIndex !== -1 && lastIndex === pos; })(ticker, ":IM"))
        return ticker.split(":IM").join(":IT");
    else if ((function (str, searchString) { var pos = str.length - searchString.length; var lastIndex = str.indexOf(searchString, pos); return lastIndex !== -1 && lastIndex === pos; })(ticker, ":LN"))
        return ticker.split(":LN").join(":UK");
    else if ((function (str, searchString) { var pos = str.length - searchString.length; var lastIndex = str.indexOf(searchString, pos); return lastIndex !== -1 && lastIndex === pos; })(ticker, ":MM"))
        return ticker.split(":MM").join(":MX");
    else if ((function (str, searchString) { var pos = str.length - searchString.length; var lastIndex = str.indexOf(searchString, pos); return lastIndex !== -1 && lastIndex === pos; })(ticker, ":BB"))
        return ticker.split(":BB").join(":BE");
    else if ((function (str, searchString) { var pos = str.length - searchString.length; var lastIndex = str.indexOf(searchString, pos); return lastIndex !== -1 && lastIndex === pos; })(ticker, ":NA"))
        return ticker.split(":NA").join(":NL");
    else if ((function (str, searchString) { var pos = str.length - searchString.length; var lastIndex = str.indexOf(searchString, pos); return lastIndex !== -1 && lastIndex === pos; })(ticker, ":AV"))
        return ticker.split(":AV").join(":AT");
    else if ((function (str, searchString) { var pos = str.length - searchString.length; var lastIndex = str.indexOf(searchString, pos); return lastIndex !== -1 && lastIndex === pos; })(ticker, ":CN"))
        return ticker.split(":CN").join(":CA");
    else if ((function (str, searchString) { var pos = str.length - searchString.length; var lastIndex = str.indexOf(searchString, pos); return lastIndex !== -1 && lastIndex === pos; })(ticker, ":DC"))
        return ticker.split(":DC").join(":DK");
    else if ((function (str, searchString) { var pos = str.length - searchString.length; var lastIndex = str.indexOf(searchString, pos); return lastIndex !== -1 && lastIndex === pos; })(ticker, ":SS"))
        return ticker.split(":SS").join(":SE");
    else if ((function (str, searchString) { var pos = str.length - searchString.length; var lastIndex = str.indexOf(searchString, pos); return lastIndex !== -1 && lastIndex === pos; })(ticker, ":PL"))
        return ticker.split(":PL").join(":PT");
    else if ((function (str, searchString) { var pos = str.length - searchString.length; var lastIndex = str.indexOf(searchString, pos); return lastIndex !== -1 && lastIndex === pos; })(ticker, ":FH"))
        return ticker.split(":FH").join(":FI");
    else if ((function (str, searchString) { var pos = str.length - searchString.length; var lastIndex = str.indexOf(searchString, pos); return lastIndex !== -1 && lastIndex === pos; })(ticker, ":ID"))
        return ticker.split(":ID").join(":IE");
    else if ((function (str, searchString) { var pos = str.length - searchString.length; var lastIndex = str.indexOf(searchString, pos); return lastIndex !== -1 && lastIndex === pos; })(ticker, ":SW"))
        return ticker.split(":SW").join(":CH");
    return ticker;
};


 function getZone (country) {
    var zones = {
        GR: "europe",
        AV: "europe",
        BB: "europe",
        DC: "europe",
        SM: "europe",
        FH: "europe",
        FP: "europe",
        GA: "europe",
        NA: "europe",
        LN: "europe",
        ID: "europe",
        IM: "europe",
        SS: "europe",
        SW: "europe",
        NO: "europe",

        US: "usa",
        CN: "usa",
        MM: "usa",

        AU: "asia",
        JP: "asia",
        HK: "asia",
        IN: 'asia'
    }
    return zones[country]
}

function getUrl(ticker, country) {
    console.log('----', ticker, country)
    if (country == '/GR') {
        country = '/XE/XETR'
    } else if (country == '/GA') {
        country = '/GR'
    }
    return "http://wsj.com/market-data/quotes/COUNTRY/TICKER".replace('TICKER', ticker).replace('/COUNTRY', country)
}