import sys
import yfinance as yf

stock = yf.Ticker(sys.argv[1])
hist = stock.history(period="max")
hist.reset_index(inplace=True)
print(hist.to_json(orient='records'))